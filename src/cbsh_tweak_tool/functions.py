import gi

gi.require_version("Gtk", "3.0")

from os import rmdir, unlink, walk, execl, getpid, system, stat, readlink
from os import path, getlogin, mkdir, makedirs, listdir
from distro import id
import os
from gi.repository import GLib, Gtk
import sys
import threading
import shutil
import psutil
import datetime
import subprocess
import logging
import time
from queue import Queue



## Variables List
pacmanCfg = "/etc/pacman.conf"
pacmanCfg_arch = "data/pacman/arch/pacman.conf"
pacmanCfgBlank_arch = "data/pacman/arch/blank/pacman.conf"



#Notifications
def show_in_app_notification(self, message):
    if self.timeout_id is not None:
        GLib.source_remove(self.timeout_id)
        self.timeout_id = None

    self.notification_label.set_markup(
        '<span foreground="white">' + message + "</span>"
    )
    self.notification_revealer.set_reveal_child(True)
    self.timeout_id = GLib.timeout_add(3000, timeOut, self)


def timeOut(self):
    close_in_app_notification(self)


def close_in_app_notification(self):
    self.notification_revealer.set_reveal_child(False)
    GLib.source_remove(self.timeout_id)
    self.timeout_id = None
