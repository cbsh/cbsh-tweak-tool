#!/usr/bin/env python3
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, GLib, Gdk, GdkPixbuf

import threading
import time
import tempfile
import os
from os import listdir
from os.path import exists, expanduser
from pathlib import Path
from wand.image import Image

import distro
import shutil
import subprocess

from time import sleep

import tweaktoolobjects as obj

global home
global user
global source
global dest
global settings
global bash_exists
global zsh_exists
global fish_exists
global defaultTextBuffer
global gymLeaderTextBuffer


home           = expanduser("~")
user           = os.path.split(home)[-1]
autostart      = home + "/.config/autostart"
pacman         = "/etc/pacman.conf"
mirrorlist     = "/etc/pacman.d/mirrorlist"
arandrCheck    = subprocess.run(["pacman", "-Qi", "arandr"], stdout=subprocess.DEVNULL)
alacrittyCheck = subprocess.run(["pacman", "-Qi", "alacritty"], stdout=subprocess.DEVNULL)
kittyCheck     = subprocess.run(["pacman", "-Qi", "kitty"], stdout=subprocess.DEVNULL)
urxvtCheck     = subprocess.run(["pacman", "-Qi", "rxvt-unicode"], stdout=subprocess.DEVNULL)

builder = Gtk.Builder()

bash_exists = '/bin/bash'
zsh_exists = '/bin/zsh'
fish_exists = '/bin/fish'

if Path(bash_exists).is_file():
    print(f'Bash is Installed')
else:
    print(f'Bash is not Installed')

if Path(zsh_exists).is_file():
    print(f'Zsh is Installed')
else:
    print(f'Zsh is not Installed')

if Path(fish_exists).is_file():
    print(f'Fish is Installed')
else:
    print(f'Fish is not Installed')

class Handler(): 
        # Autostart Tab
        autostartFiles = [x.replace(".desktop", "") for x in listdir(autostart)]

        for autostartFile in autostartFiles:
                autostartRow = Gtk.ListBoxRow()
                autostartGrid = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
                autostartGrid.set_spacing(10)
                autostartRowLabel = Gtk.Label()
                autostartRowBtn = Gtk.Button(None, image=Gtk.Image(stock=Gtk.STOCK_DELETE))
                autostartGrid.pack_start(autostartRowLabel,False,False,0)
                autostartGrid.pack_end(autostartRowBtn,False,False,0)
                autostartRow.add(autostartGrid)
                autostartListBox.add(autostartRow)
                autostartRowLabel.set_text(autostartFile)
                
                
                
                # Autostart File Browser
                def on_execFileChooser_clicked(self, widget):
                        execDialog = Gtk.FileChooserDialog(
                                title="Choose a File", action=Gtk.FileChooserAction.OPEN
                        )
                        execDialog.set_select_multiple(False)
                        execDialog.set_show_hidden(False)
                        execDialog.set_current_folder(home)
                        execDialog.add_buttons(
                                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, "Open", Gtk.ResponseType.OK
                        )
                        
                        execDialog.connect("response", self.execResponse)
                        execDialog.show()
                        
                def execResponse(self, execDialog, response):
                        if response == Gtk.ResponseType.OK:
                                print(execDialog.get_filenames())
                                folderName = execDialog.get_filename()
                                autostartCmdEntry.set_text(folderName[0])
                                execDialog.destroy()
                        elif response == Gtk.ResponseType.CANCEL:
                                execDialog.destroy()

            # Adding Autostart Buttons


            # TERMINAL SHELL TAB
                if Path(bash_exists).is_file():
                        bashInstalledLabel.set_text("Bash: Installed")
                        bashShellRadio.set_sensitive(True)
                else:
                        bashInstalledLabel.set_text("Bash: Not Installed")
                        bashShellRadio.set_sensitive(False)
                    
                if Path(zsh_exists).is_file():
                        zshInstalledLabel.set_text("Zsh: Installed")
                        zshShellRadio.set_sensitive(True)
                else:
                        zshInstalledLabel.set_text("Zsh: Not Installed")
                        zshShellRadio.set_sensitive(False)

                if Path(fish_exists).is_file():
                        fishInstalledLabel.set_text("Fish: Installed")
                        fishShellRadio.set_sensitive(True)
                else:
                        fishInstalledLabel.set_text("Fish: Not Installed")
                        fishShellRadio.set_sensitive(False)

                @staticmethod        
                def bashRadioClicked():                                
                        if Path(bash_exists).is_file():
                                print(f'/bin/bash exists.')
                                changeShellLabel.set_text("Shell changed to bash")
                                subprocess.run(["pkexec", "chsh", user, "-s", "/bin/bash"])
                        else:
                                changeShellLabel.set_text(shell, "is not installed.")

                @staticmethod                
                def zshRadioClicked():
                        if Path(zsh_exists).is_file():
                                print(f'/bin/zsh exists.')
                                changeShellLabel.set_text("Shell changed to zsh")
                                subprocess.run(["pkexec", "chsh", user, "-s", "/bin/zsh"])
                        else:
                                print(shell, "is not installed.")

                @staticmethod                
                def fishRadioClicked():
                        if Path(fish_exists).is_file():
                                print(f'/bin/fish exists.')
                                changeShellLabel.set_text("Shell changed to fish")
                                subprocess.run(["pkexec", "chsh", user, "-s", "/bin/fish"])
                        else:
                                print(shell, "is not installed.")

                    
                def on_changeShellBtn_clicked(self, widget):
                        if noChangeShellRadio.get_active():
                                changeShellLabel.set_text("Shell not changed")

                                
                        if fishShellRadio.get_active():
                                print ("Setting fish as", user,"'s shell")
                                self.fishRadioClicked()

                        if zshShellRadio.get_active():
                                print ("Setting zsh as", user,"'s shell")
                                self.zshRadioClicked()

                        if bashShellRadio.get_active():
                                print ("Setting bash as", user,"'s shell")
                                self.bashRadioClicked()
                                



                             
                # DEFAULT THEME TAB
                def on_previewThemeBtn_clicked(self, widget):
                        if noChangeThemeRadio.get_active():
                                themePreviewImg.set_from_file("CBSHTweakTool.png")
                        if cbshVibrantRadio.get_active():
                                themePreviewImg.set_from_file("theme_data/cbshVibrant.png")
                        if cbshBlackHoleRadio.get_active():
                                themePreviewImg.set_from_file("theme_data/cbshBlackHole.png")
                        if doomOneRadio.get_active():
                                themePreviewImg.set_from_file("theme_data/cbshDoomOne.png")
                        if doomDraculaRadio.get_active():
                                themePreviewImg.set_from_file("theme_data/cbshDoomDracula.png")
                        if doomSolarizedDarkRadio.get_active():
                                themePreviewImg.set_from_file("theme_data/cbshDoomSolarizedDark.png")

                @threaded                
                def ThemeChangeLogic(self, theme, msg, term):
                        while theme.poll() is None:
                               for line in theme.stdout:
                                       textview = term.get_buffer().insert_at_cursor(line)
                                       sleep(0.1)
                             
                        if theme.returncode == 0:
                                successMsg = builder.get_object("successMsg")
                                successMsg.format_secondary_text(msg)
                                successMsg.add_button("Ok", Gtk.ResponseType.OK)
                                successMsg.run()
                                if successMsg == Gtk.ResponseType.OK:
                                        print("dialog closed")
                                successMsg.destroy()
                                sleep(0.2)
                                changeThemeLabel.set_text("Choose a Theme From Above")
                        
                
                @threaded
                def on_changeThemeBtn_clicked(self, widget):
                        if noChangeThemeRadio.get_active():
                                changeThemeLabel.set_text("Theme not changed")

                        if cbshVibrantRadio.get_active():
                                changeThemeLabel.set_text("Changing theme to CBSH Vibrant...")
                                themeVibrant = subprocess.Popen(
                                        "./theme_data/theme-cbsh-vibrant",
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.STDOUT,
                                        universal_newlines=True, shell=True, bufsize=1)
                                self.ThemeChangeLogic(themeVibrant, "Theme Successfully Changed to CBSH Vibrant", themeTerminalOutput)
                                
                        if cbshBlackHoleRadio.get_active():
                                changeThemeLabel.set_text("Changing theme to CBSH Black Hole...")
                                themeBlackHole = subprocess.Popen(
                                        "./theme_data/theme-cbsh-black-hole",
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.STDOUT,
                                        universal_newlines=True, shell=True, bufsize=1)
                                self.ThemeChangeLogic(themeBlackHole, "Theme Successfully Changed to CBSH Black Hole", themeTerminalOutput)

                        if doomOneRadio.get_active():
                                changeThemeLabel.set_text("Changing theme to Doom One...")
                                themeDoomOne = subprocess.Popen(
                                        "./theme_data/theme-cbsh-doom-one",
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.STDOUT,
                                        universal_newlines=True, shell=True, bufsize=1)
                                self.ThemeChangeLogic(themeDoomOne, "Theme Successfully Changed to Doom One", themeTerminalOutput)
                            
                        if doomDraculaRadio.get_active():
                                changeThemeLabel.set_text("Changing theme to Doom Dracula...")
                                themeDoomDracula = subprocess.Popen(
                                        "./theme_data/theme-cbsh-doom-dracula",
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.STDOUT,
                                        universal_newlines=True, shell=True, bufsize=1)
                                self.ThemeChangeLogic(themeDoomDracula, "Theme Successfully Changed to Doom Dracula", themeTerminalOutput)

                        if doomSolarizedDarkRadio.get_active():
                                changeThemeLabel.set_text("Changing theme to Doom Solarized Dark...")
                                themeDoomSolarizedDark = subprocess.Popen(
                                        "./theme_data/theme-cbsh-vibrant",
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.STDOUT,
                                        universal_newlines=True, shell=True, bufsize=1)
                                self.ThemeChangeLogic(themeDoomSolarizedDark, "Theme Successfully Changed to Doom Solarized Dark", themeTerminalOutput)
                                        

            # POKEMON GYM LEADER THEME TAB
                def on_changeGymLeaderThemeBtn_clicked(self, widget):
                        if noChangeGymLeaderThemeRadio.get_active():
                                changeGymLeaderThemeLabel.set_text("Theme not changed")

                        if dianthaRadio.get_active():
                                changeGymLeaderThemeLabel.set_text("Theme changed to Diantha")

                        if elesaRadio.get_active():
                                changeGymLeaderThemeLabel.set_text("Theme changed to Elesa")

                        if elesaAltRadio.get_active():
                                changeGymLeaderThemeLabel.set_text("Theme changed to Elesa (B2/W2 Outfit)")

                        if shauntalRadio.get_active():
                                changeGymLeaderThemeLabel.set_text("Theme changed to Shauntal")

                        if korrinaRadio.get_active():
                                changeGymLeaderThemeLabel.set_text("Theme changed to Korrina")

                        if roxieRadio.get_active():
                                changeGymLeaderThemeLabel.set_text("Theme changed to Roxie")

                        if skylaRadio.get_active():
                                changeGymLeaderThemeLabel.set_text("Theme changed to Skyla")

                        if valerieRadio.get_active():
                                changeGymLeaderThemeLabel.set_text("Theme changed to Valerie")

                        if cynthiaRadio.get_active():
                                changeGymLeaderThemeLabel.set_text("Theme changed to Cynthia")

                        if flanneryRadio.get_active():
                                changeGymLeaderThemeLabel.set_text("Theme changed to Flannery")

                        if sidneyradio.get_active():
                                changeGymLeaderThemeLabel.set_text("Theme changed to Sidney")



                                
            #RESOLUTION TAB
                def on_launchArandrBtn_clicked(self, widget):
                        subprocess.run(["arandr"])

                def on_updateXrandrBtn_clicked(self, widget):
                        options = updateXrandrEntry.get_text()
                        splitoptions = options.split()
                        subprocess.run(splitoptions)
                        subprocess.run(["sed", "-i", f"4c{options}" , home + "/.config/qtile/autostart.sh"])

            # TERMINALS STACK
                def on_alacrittyInstallBtn_clicked(self,widget):
                        subprocess.run(["sudo", "-A" ,"pacman", "-S", "--noconfirm", "alacritty"])
                        
                def on_kittyInstallBtn_clicked(self,widget):
                        subprocess.run(["sudo", "-A" ,"pacman", "-S", "--noconfirm", "kitty"])
                        
                def on_urxvtInstallBtn_clicked(self,widget):
                        subprocess.run(["sudo", "-A", "pacman", "-S", "--noconfirm", "rxvt-unicode"])


            # GRUB THEMES STACK
                def on_grubApplyTimeoutBtn_clicked(self, widget):
                        print("lol")

                def get_grub_wallpapers():
                        if path.isdir("/boot/grub/themes/CBSH"):
                                lists = listdir("/boot/grub/themes/CBSH")
                                
                                rems = [
                                        "select_e.png",
                                        "terminal_box_se.png",
                                        "select_c.png",
                                        "terminal_box_c.png",
                                        "terminal_box_s.png",
                                        "select_w.png",
                                        "terminal_box_nw.png",
                                        "terminal_box_w.png",
                                        "terminal_box_ne.png",
                                        "teminal_box_sw.png",
                                        "terminal_box_n.png",
                                        "terminal_box_e.png",
                                ]
                            
                                ext = [".png", ".jpeg", ".jpg"]
                            
                                new_list = [x for x in lists if x not in rems for y in ext if y in x]
                            
                                new_list.sort()
                                return new_list

                def pop_themes_grub(self, combo, lists, start):
                        if fn.path.isfile(fn.grub_theme_conf):
                                combo.get_model().clear()
                                with open(fn.grub_theme_conf, "r", encoding="utf-8") as f:
                                        listss = f.readlines()
                                        f.close()

                                        
                                        val = fn.get_position(listss, "desktop-image: ")
                                        # bg_image = listss[val].split(" ")[1].replace('"', "").strip()
                                        for x in self.fb.get_children():
                                                self.fb.remove(x)

                                                for x in lists:
                                                        pb = GdkPixbuf.Pixbuf().new_from_file_at_size(
                                                    "/boot/grub/themes/CBSH/" + x, 128, 128
                                                        )
                                                        pimage = Gtk.Image()
                                                        pimage.set_name("/boot/grub/themes/CBSH/" + x)
                                                        pimage.set_from_pixbuf(pb)
                                                        self.fb.add(pimage)
                                                        pimage.show_all()

                def on_import_wallpaper(self, widget):
                        text = self.tbimage.get_text()
                        if len(text) > 1:
                                print(fn.path.basename(text))
                                fn.shutil.copy(text, "/boot/grub/themes/CBSH/" + fn.path.basename(text))
                                self.pop_themes_grub(self.grub_theme_combo, fn.get_grub_wallpapers(), False)
                        else:
                                print("First search for a wallpaper")
                                fn.show_in_app_notification(self, "First select an image")





                #Pacman Settings Stack
                


                #User Creation Stack
                def on_accTypeCombo_changed(self, widget):
                        print("do stuff here")

            
                 
                                
                def on_MainExitBtn_clicked(self, widget):
                        Gtk.main_quit()

splashWindow.show_all()


builder.connect_signals(Handler())
mainWin = builder.get_object("MainWindow")
mainWin.connect("destroy", Gtk.main_quit)

def do_work(splashWindow, mainWin):
    sleep(3)
    splashWindow.destroy()
    mainWin.show_all()

worker = threading.Thread(target=lambda: do_work(splashWindow, mainWin))
worker.start()
Gtk.main()
